let Carousel = Object.create({});

Carousel.importCommands = function() {
  browser.addCommand('scrollToMid', function(selector) {
    browser.execute(function(arg) {
      let element = (arg instanceof Element) ? arg : document.querySelector(arg);
      let elementRect = element.getBoundingClientRect();
      let absElementTop = elementRect.top + window.pageYOffset;
      let middle = absElementTop - (window.innerHeight / 2);

      window.scrollTo(0, middle);
    }, selector);
    browser.pause(750);
  });
};


/**
 * Resizes the browser window if necessary
 * @param  {Number} width Desired pixel width of window
 */
Carousel.setViewport = function(width) {
  let size = browser.windowHandleSize();

  if (size.value.width !== width) {
    console.log(`resizing from ${size.value.width} to ${width}`);
    browser.windowHandleSize({width: width, height: 800});
    browser.pause(1000);
  }
};


/**
 * Asserts that the number of visible slides matches
 * @param  {Object} settings   Data for entire test suite
 * @param  {Number} index      Index of slider being tested
 * @param  {Object} test     Data for the slider at a certain breakpoint
 */
Carousel.checkColumns = function(settings, index, test) {
  let slider = browser.$$(settings.selector.slider)[index];
  let slides = slider.$$(settings.selector.slide).filter(function(slide) {
    return slide.isVisible();
  });

  expect(slides.length).to.equal(test.slidesToShow, 'Incorrect column count');
};


/**
 * Retrieves active controls for given slider
 * @param  {Object} settings     Data for entire test suite
 * @param  {Number} index        Index of slider being tested
 * @param  {String} control_type Name of controls (arrows or dots)
 * @return {Array}               List of visible controls
 */
Carousel.getControls = function(settings, index, control_type) {
  let slider = browser.$$(settings.selector.slider)[index];
  let controls = slider.$$(settings.selector[control_type]).filter(function(ctrl) {
    return ctrl.isVisible();
  });

  return controls;
};


/**
 * Asserts that the slider can transition between the first and last slides
 * @param  {Object}  settings     Data for entire test suite
 * @param  {Number}  index        Index of slider being tested
 * @param  {Boolean} visible      Represents expected visibility
 * @param  {String}  control_type Name of controls (arrows or dots)
 */
Carousel.checkControls = function(settings, index, visible, control_type) {
  let controls = Carousel.getControls(settings, index, control_type);
  let controlsVisible = controls.length !== 0;

  expect(controlsVisible).to.equal(visible, `${control_type} visibility is incorrect`);
};


/**
 * Asserts that the slider can transition between the first and last slides
 * @param  {Object} settings Data for entire test suite
 * @param  {Number} index    Index of slider being tested
 * @param  {Object} test     Data for the slider at a certain breakpoint
 */
Carousel.checkInfinite = function(settings, index, test) {
  const LEFT = 'ArrowLeft';
  let slider = browser.$$(settings.selector.slider)[index];
  let slides = slider.$$(settings.selector.slide);
  let last = slides[slides.length - 1];
  let msg = `Slider should ${test.infinite ? '' : 'not'} reveal last slide`;

  expect(last.isVisible()).to.be.false('Last slide should be hidden');

  slides[0].click();
  browser.keys(LEFT);
  browser.pause(1000);
  expect(last.isVisible()).to.equal(test.infinite, msg);
};


/**
 * Asserts that clicking an arrow button reveals the appropriate slide
 * @param  {Object} settings Data for entire test suite
 * @param  {Number} index    Index of slider being tested
 * @param  {Object} test     Data for the slider at a certain breakpoint
 */
Carousel.checkArrows = function(settings, index, test) {
  let slider = browser.$$(settings.selector.slider)[index];
  let slides = slider.$$(settings.selector.slide);
  let prev = slider.$(settings.selector.prev);
  let next = slider.$(settings.selector.next);
  let hiddenSlide = slides[test.slidesToShow];
  let slideId = `#slick-slide${index}0`;

  browser.scrollToMid(slideId);

  expect(slides[0].isVisible()).to.be.true('First slide should be visible');
  expect(hiddenSlide.isVisible()).to.be.false(`Slide index ${test.slidesToShow} should be hidden before pressing next`);

  next.click();
  browser.pause(800);
  expect(slides[0].isVisible()).to.be.true('First slide should be hidden');
  expect(hiddenSlide.isVisible()).to.be.true(`Slide index ${test.slidesToShow} should be visible`);

  prev.click();
  browser.pause(800);
  expect(slides[0].isVisible()).to.be.true('First slide should be visible again');
  expect(hiddenSlide.isVisible()).to.be.false(`Slide index ${test.slidesToShow} should be hidden again`);
};


/**
 * Asserts that clicking a dot button reveals the appropriate slide
 * @param  {Object} settings Data for entire test suite
 * @param  {Number} index    Index of slider being tested
 * @param  {Object} test     Data for the slider at a certain breakpoint
 */
Carousel.checkDots = function(settings, index, test) {
  let slider = browser.$$(settings.selector.slider)[index];
  let slides = slider.$$(settings.selector.slide);
  let dots = Carousel.getControls(settings, index, 'dots');
  let next;

  dots.forEach(function(dot, i) {
    if (i === 0) {
      browser.scrollToMid(dot);
      slides[0].click();
      browser.keys('ArrowRight');
      browser.pause(1200);
    }
    next = test.slidesToShow * i;

    browser.scrollToMid(dot);
    dot.click();
    browser.pause(1200);
    expect(slides[next].isVisible()).to.be.true(`Dot ${i}. Slide ${next} should be visible`);
  });

};

module.exports = Carousel;
