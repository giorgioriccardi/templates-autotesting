const Links = Object.create({});

/**
 * Adds custom commands related to testing links
 */
Links.importCommands = function() {
  /**
   * Asserts that retrieved link has the expected text value
   * @param  {WebElement} element Element retrieved by browser.elements()
   * @param  {Object} test    Object representing test case
   */
  browser.addCommand('checkLinkText', function(element, test) {
    let title = element.getText();
    expect(title).to.equal(test.text, 'Link is titled different');
  });


  /**
   * Asserts that retrieved link goes to the expected URL
   * @param  {WebElement} element Element retrieved by browser.elements()
   * @param  {Object} test    Object representing test case
   */
  browser.addCommand('checkLinkHref', function(element, test) {
    // If external link, expected to open a new tab
    element.click().pause(1000);

    if (test.external === true) {
      // External links should open in new tab, then switch back afterwards
      let handles = this.windowHandles();
      this.switchTab(handles.value[1]).pause(1000);

      expect(this.getUrl()).to.equal(test.href, 'Link destination is different');
      this.close();
    } else {
      expect(this.getUrl()).to.equal(test.href, 'Link destination is different');
    }
  });


  /**
   * Asserts that retrieved link has a href that matches the expected value
   * @param  {WebElement} element Element retrieved by browser.elements()
   * @param  {Object} test    Object representing test case
   */
  browser.addCommand('matchLinkHref', function(element, test) {
    let href = element.getAttribute('href');
    expect(href).to.equal(test.href);
  });


  /**
   * Asserts that retrieved link eventually loads a page whose URL contains the expected value
   * @param  {WebElement} element Element retrieved by browser.elements()
   * @param  {Object} test    Object representing test case
   */
  browser.addCommand('waitForUrl', function(element, test) {
    element.click();

    if (test.external === true) {
      let handles = this.windowHandles();
      this.switchTab(handles.value[1]).pause(1000);
    }

    browser.waitUntil(function() {
      return browser.getUrl().indexOf(test.href) > -1;
    }, 5000, 'Expected substring in url within timeout interval');
  });

};


/**
 * Evaluates one test case based on the given selector
 * @param  {Object} test     Object representing test case
 * @param  {String} selector CSS selector for element being tested
 */
Links.testLink = function(test, selector) {
  let description = test.description ||
                    `\'${test.text}\' link should go to \'${test.href}\'`;

  it(description, function() {
    let element = browser.elements(selector).value[test.index];

    browser.checkLinkText(element, test);

    switch (test.custom) {
      case 'matchLinkHref':
        browser.matchLinkHref(element, test);
        break;
      case 'waitForUrl':
        browser.waitForUrl(element, test);
        break;
      default:
        browser.checkLinkHref(element, test);
        break;
    }
  });
};


module.exports = Links;
