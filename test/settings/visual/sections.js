const tests = {
  suite: 'sections',
  selector: '.grid--pl',
  cases: [
    {
      description: 'first-test',
      options: {
        name: 'first'
      }
    },
    {
      description: 'second-test',
      options: {
        name: 'second'
      }
    },
    {
      description: 'third-test',
      options: {
        name: 'third'
      }
    }
  ]
};


module.exports = tests;
