const tests = {
  suite: 'footer links',
  selector: '.footer-inner a',
  cases: [
    {
      index: 0,
      text: '',
      href: 'http://camppacific.com/',
      external: true
    },
    {
      index: 1,
      text: '',
      href: 'http://ca.citizenrelations.com/',
      external: true
    },
    {
      description: 'href should exactly match \'tel:+17783318340\'',
      index: 2,
      text: '+1 778-331-8340',
      href: 'tel:+17783318340',
      custom: 'matchLinkHref'
    },
    {
      index: 3,
      text: 'TWITTER',
      href: 'https://twitter.com/camppacific',
      external: true
    },
    {
      index: 4,
      text: 'FACEBOOK',
      href: 'https://www.facebook.com/camppacificvancouver',
      external: true
    },
    {
      index: 5,
      text: 'INSTAGRAM',
      href: 'https://www.instagram.com/camppacific/',
      external: true
    },
    {
      description: 'link should go to main/redirect page with url containing \'company-beta%2F3650562%2F%3FpathWildcard%3D3650562\'',
      index: 6,
      text: 'LINKEDIN',
      href: 'company-beta%2F3650562%2F%3FpathWildcard%3D3650562',
      external: true,
      custom: 'waitForUrl'
    }
  ]
};

module.exports = tests;
