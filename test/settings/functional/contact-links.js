const tests = {
  suite: 'contact links',
  selector: '.contact-block__contact-info a',
  cases: [
    {
      description: 'href should exactly match \'tel:+16047883150\'',
      index: 0,
      text: '+1 604-788-3150',
      href: 'tel:+16047883150',
      custom: 'matchLinkHref'
    },
    {
      description: 'href should exactly match \'mailto:mathew.stockton@camppacific.com\'',
      index: 1,
      text: 'mathew.stockton@camppacific.com',
      href: 'mailto:mathew.stockton@camppacific.com',
      custom: 'matchLinkHref'
    },
    {
      description: 'Signal button link should exactly match \'mailto:mathew.stockton@camppacific.com\'',
      index: 2,
      text: 'Connect with us',
      href: 'mailto:mathew.stockton@camppacific.com',
      custom: 'matchLinkHref'
    }
  ]
};

module.exports = tests;
