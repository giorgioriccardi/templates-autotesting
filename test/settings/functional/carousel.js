let settings = {
  suite: 'carousels',
  selector: {
    slider: '.slick-slider',
    slide: '.slick-slide:not(.slick-cloned)',
    dotted: '.slick-dotted',
    dots: '.slick-dots button',
    arrows: '.slick-arrow',
    prev: '.slick-prev',
    next: '.slick-next'
  },
  pages: [{
    title: 'real time traffic page',
    url: '/patterns/05-pages-03-real-time-traffic/05-pages-03-real-time-traffic.rendered.html',
    sliders: [{
      index: 0,
      description: '"how the service works" section',
      breakpoints: [{
        width: 992,
        slides: 3,
        slidesToShow: 3,
        infinite: false,
        arrows: false,
        dots: false
      }, {
        width: 768,
        slides: 3,
        slidesToShow: 3,
        infinite: false,
        arrows: false,
        dots: false
      }, {
        width: 544,
        slides: 3,
        slidesToShow: 1,
        infinite: true,
        arrows: false,
        dots: true
      }]
    }, {
      index: 1,
      description: '"Guidance over navigation" section',
      breakpoints: [{
        width: 992,
        slides: 6,
        slidesToShow: 2,
        infinite: true,
        arrows: false,
        dots: true
      }, {
        width: 768,
        slides: 6,
        slidesToShow: 2,
        infinite: true,
        arrows: false,
        dots: true
      }, {
        width: 544,
        slides: 6,
        slidesToShow: 1,
        infinite: true,
        arrows: false,
        dots: true
      }]
    }, {
      index: 2,
      description: '"More solutions" section',
      breakpoints: [{
        width: 992,
        slides: 7,
        slidesToShow: 3,
        infinite: true,
        arrows: false,
        dots: true
      }, {
        width: 768,
        slides: 7,
        slidesToShow: 2,
        infinite: true,
        arrows: false,
        dots: true
      }, {
        width: 544,
        slides: 7,
        slidesToShow: 1,
        infinite: true,
        arrows: false,
        dots: true
      }]
    }]
  }]
};

module.exports = settings;
