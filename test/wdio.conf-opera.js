let merge = require('deepmerge');
let wdioConf = require('./wdio.conf.js');

exports.config = merge(wdioConf.config, {
  path: '/',
  capabilities: [{
    browserName: 'opera',
    maxInstances: 5
    // specs: [
    // ],
    // exclude: [
    // ]
  }]
});
