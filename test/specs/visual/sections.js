/* global compareElement */

const tests = require('../../settings/visual/sections');
const browserName = browser.desiredCapabilities.browserName;

describe(`${browserName}: ${tests.suite}`, function() {
  beforeEach(function() {
    browser.url('');
  });

  tests.cases.forEach(function(test) {
    it(`${test.description} should look similar`, function() {
      compareElement(tests.selector, test.options);
    });
  });

});
