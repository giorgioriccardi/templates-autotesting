const Links = require('../../util/links');
const tests = require('../../settings/functional/footer-links.js');
const browserName = browser.desiredCapabilities.browserName;

describe(`${browserName}: ${tests.suite}`, function() {

  before(function() {
    Links.importCommands();
  });

  beforeEach(function() {
    browser.url('/');
    browser.waitForVisible(tests.selector);
  });

  tests.cases.forEach(function(test) {
    Links.testLink(test, tests.selector);
  });


});
