/* global compareDocument */

const Carousel = require('../../util/carousel');
const tests = require('../../settings/functional/carousel');
const browserName = browser.desiredCapabilities.browserName;
let width;
let description;

Carousel.importCommands();

describe(`${browserName}: ${tests.suite},`, function() {
  tests.pages.forEach(function(page) {
    describe(`${page.title},`, function() {
      before(function() {
        browser.url(page.url);
        browser.waitForVisible(tests.selector.slide);
      });

      page.sliders.forEach(function(slider) {
        describe(`${slider.description}`, function() {
          slider.breakpoints.forEach(function(breakpoint) {
            width = `${breakpoint.width}px.`;

            description = `${width} arrows should be ${breakpoint.arrows ? 'enabled' : 'disabled'}`;
            it(description, function() {
              Carousel.setViewport(breakpoint.width);
              Carousel.checkControls(tests, slider.index, breakpoint.arrows, 'arrows');
            });

            description = `${width} dots should be ${breakpoint.dots ? 'enabled' : 'disabled'}`;
            it(description, function() {
              Carousel.setViewport(breakpoint.width);
              Carousel.checkControls(tests, slider.index, breakpoint.dots, 'dots');
            });

            description = `${width} ${breakpoint.slidesToShow} column(s) should be visible`;
            it(description, function() {
              browser.refresh();
              Carousel.setViewport(breakpoint.width);
              Carousel.checkColumns(tests, slider.index, breakpoint);
            });

            description = `${width} infinite scroll should be ${breakpoint.infinite ? 'enabled' : 'disabled'}`;
            it(description, function() {
              if (breakpoint.slidesToShow < breakpoint.slides) {
                browser.refresh();
                Carousel.setViewport(breakpoint.width);
                Carousel.checkInfinite(tests, slider.index, breakpoint);
              } else {
                this.skip();
              }
            });

            description = `${width} arrow controls should change slides`;
            it(description, function() {
              let controls = Carousel.getControls(tests, slider.index, 'arrows');

              if (controls.length === 0 || breakpoint.slidesToShow >= breakpoint.slides) {
                this.skip();
              }

              browser.refresh();
              Carousel.setViewport(breakpoint.width);
              Carousel.checkArrows(tests, slider.index, breakpoint);
            });

            description = `${width} dot controls should change slides`;
            it(description, function() {
              browser.refresh();
              Carousel.setViewport(breakpoint.width);

              let controls = Carousel.getControls(tests, slider.index, 'dots');

              if (controls.length === 0) {
                this.skip();
              } else {
                Carousel.checkDots(tests, slider.index, breakpoint);
              }
            });

          }); // slider.breakpoints.forEach
        }); // define test suite per slider
      }); // page.sliders.forEach
    }); // define test suite per page
  }); // tests.pages.forEach
}); // define test suite per browser
