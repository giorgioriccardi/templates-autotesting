require('dotenv').config();
let merge = require('deepmerge');
let wdioConf = require('./wdio.conf.js');

exports.config = merge(wdioConf.config, {
  // =====================
  // Server Configurations
  // =====================
  // Host address of the running Selenium server. This information is usually obsolete as
  // WebdriverIO automatically connects to localhost. Also, if you are using one of the
  // supported cloud services like Sauce Labs, Browserstack, or Testing Bot you don't
  // need to define host and port information because WebdriverIO can figure that out
  // according to your user and key information. However, if you are using a private Selenium
  // backend you should define the host address, port, and path here.
  //
  protocol: 'http', // Bug workaround. Affects v4.6.2 https://github.com/webdriverio/webdriverio/issues/1857
  host: 'hub.crossbrowsertesting.com',
  port: 80,
  user: process.env.CROSS_BROWSER_TESTING_USER,
  key: process.env.CROSS_BROWSER_TESTING_KEY,

  maxInstances: 5, // https://crossbrowsertesting.com/pricing Pro Plan is max 5

  // Can get capability details from CrossBrowserTesting's Selenium Wizard
  // https://app.crossbrowsertesting.com/selenium/run
  capabilities: [{
    name: 'Microsoft Edge',
    build: '1.0',

    browser_api_name: 'Edge15',
    os_api_name: 'Win10',
    screen_resolution: '1366x768',
    browserName: 'microsoft edge',

    record_video: 'true',
    record_network: 'true',

    // Browser-specific tests
    // specs: [
    // ],
    // exclude: [
    // ]
  }],

  // Level of logging verbosity: silent | verbose | command | data | result | error
  logLevel: 'result',

  // Set a base URL in order to shorten url command calls. If your url parameter starts
  // with "/", then the base url gets prepended.
  // baseUrl: 'http://10.44.26.49:3000/pattern-lab/public/styleguide/html/styleguide.html',
  baseUrl: process.env.BASE_URL_CBT,

  services: ['visual-regression'],
  seleniumInstallArgs: {},
  seleniumArgs: {},

  reporters: ['dot'],
});
