// This is the main configuration file that is shared between all the environment configs
/* global assertDiff compareElement compareDocument */

require('dotenv').config();
let path = require('path');
let VisualRegressionCompare = require('wdio-visual-regression-service/compare');

/**
 * Creates filename for individual screenshot
 * @param  {String} basePath Path for all screenshots
 * @return {String}          Path to save new screenshot
 */
function getScreenshotName(basePath) {
  return function(context) {
    // var testName = context.test.title;
    let testName = context.options.name;
    let browserVersion = parseInt(/\d+/.exec(context.browser.version)[0]);
    let browserName = context.browser.name;
    let browserViewport = context.meta.viewport;
    let browserWidth = browserViewport.width;
    let browserHeight = browserViewport.height;

    return path.join(basePath, `${browserName}_v${browserVersion}/${testName}_${browserWidth}x${browserHeight}.png`);
  };
}

/**
 * Creates filename for reference screenshot
 * @param  {String} basePath Path for all screenshots
 * @return {String}          Path to save reference screenshot
 */
function getRefScreenshotName(basePath) {
  return function(context) {
    let testName = context.options.name;
    let browserViewport = context.meta.viewport;
    let browserWidth = browserViewport.width;
    let browserHeight = browserViewport.height;

    return path.join(basePath, `${testName}_${browserWidth}x${browserHeight}.png`);
  };
}


exports.config = {
  //
  // ==================
  // Specify Test Files
  // ==================
  // Define which test specs should run. The pattern is relative to the directory
  // from which `wdio` was called. Notice that, if you are calling `wdio` from an
  // NPM script (see https://docs.npmjs.com/cli/run-script) then the current working
  // directory is where your package.json resides, so `wdio` will be called from there.
  //
  specs: [
    'test/specs/**/*.js'
  ],
  // Patterns to exclude.
  exclude: [
    // 'path/to/excluded/files'
  ],
  // Subsets of tests to run
  suites: {
    functional: [
      'test/specs/functional/*.js'
    ],
    visual: [
      'test/specs/visual/*.js'
    ],
  },
  // Path to webdriver server.  This needs to be set for opera
  // path: '/',
  //
  // ============
  // Capabilities
  // ============
  // Define your capabilities here. WebdriverIO can run multiple capabilities at the same
  // time. Depending on the number of capabilities, WebdriverIO launches several test
  // sessions. Within your capabilities you can overwrite the spec and exclude options in
  // order to group specific specs to a specific capability.
  //
  // First, you can define how many instances should be started at the same time. Let's
  // say you have 3 different capabilities (Chrome, Firefox, and Safari) and you have
  // set maxInstances to 1; wdio will spawn 3 processes. Therefore, if you have 10 spec
  // files and you set maxInstances to 10, all spec files will get tested at the same time
  // and 30 processes will get spawned. The property handles how many capabilities
  // from the same test should run tests.
  //
  maxInstances: 10,
  //
  // If you have trouble getting all important capabilities together, check out the
  // Sauce Labs platform configurator - a great tool to configure your capabilities:
  // https://docs.saucelabs.com/reference/platforms-configurator
  //
  // maxInstances can get overwritten per capability. So if you have an in-house Selenium
  // grid with only 5 firefox instances available you can make sure that not more than
  // 5 instances get started at a time.
  //
  // Add specs or exclude lists to customize tests per browser
  capabilities: [{
    browserName: 'chrome',
    maxInstances: 5
    // specs: [
    // ],
    // exclude: [
    // ]
  }, {
    browserName: 'firefox',   // If firefox is first, it breaks
    maxInstances: 5,
    // specs: [
    // ],
    // exclude: [
    // ]
  }, {
    browserName: 'chrome',
    maxInstances: 5,
    chromeOptions: {
      mobileEmulation: {
        deviceMetrics: {
          width: 360,
          height: 640
        }
      }
    }
  }],
  //
  // ===================
  // Test Configurations
  // ===================
  // Define all options that are relevant for the WebdriverIO instance here
  //
  // By default WebdriverIO commands are executed in a synchronous way using
  // the wdio-sync package. If you still want to run your tests in an async way
  // e.g. using promises you can set the sync option to false.
  sync: true,
  //
  // Level of logging verbosity: silent | verbose | command | data | result | error
  logLevel: 'silent',
  //
  // Enables colors for log output.
  coloredLogs: true,
  //
  // If you only want to run your tests until a specific amount of tests have failed use
  // bail (default is 0 - don't bail, run all tests).
  bail: 0,
  //
  // Saves a screenshot to a given path if a command fails.
  screenshotPath: './test/error-shots/',
  //
  // Set a base URL in order to shorten url command calls. If your url parameter starts
  // with "/", then the base url gets prepended.
  baseUrl: process.env.BASE_URL,

  // NOTE: The content in patternlab in enclosed in an iframe so we have to target
  // the specific html page the iframe is showing.
  // baseUrl: 'http://localhost:3000/pattern-lab/public/styleguide/html/styleguide.html',

  //
  // Default timeout for all waitFor* commands.
  waitforTimeout: 20000,
  //
  // Default timeout in milliseconds for request
  // if Selenium Grid doesn't send response
  connectionRetryTimeout: 90000,
  //
  // Default request retries count
  connectionRetryCount: 3,
  //
  // Initialize the browser instance with a WebdriverIO plugin. The object should have the
  // plugin name as key and the desired plugin options as properties. Make sure you have
  // the plugin installed before running any tests. The following plugins are currently
  // available:
  // WebdriverCSS: https://github.com/webdriverio/webdrivercss
  // WebdriverRTC: https://github.com/webdriverio/webdriverrtc
  // Browserevent: https://github.com/webdriverio/browserevent
  // plugins: {
  //     webdrivercss: {
  //         screenshotRoot: 'my-shots',
  //         failedComparisonsRoot: 'diffs',
  //         misMatchTolerance: 0.05,
  //         screenWidth: [320,480,640,1024]
  //     },
  //     webdriverrtc: {},
  //     browserevent: {}
  // },
  //
  // Test runner services
  // Services take over a specific job you don't want to take care of. They enhance
  // your test setup with almost no effort. Unlike plugins, they don't add new
  // commands. Instead, they hook themselves up into the test process.
  services: ['selenium-standalone', 'visual-regression'],
  seleniumLogs: './test/logs',
  seleniumInstallArgs: {
    version: '3.4.0',
    drivers: {
      firefox: {
        version: '0.18.0'
      }
    }
  },
  seleniumArgs: {
    version: '3.4.0',
    drivers: {
      firefox: {
        version: '0.18.0'
      }
    }
  },
  visualRegression: {
    compare: new VisualRegressionCompare.LocalCompare({
      referenceName: getRefScreenshotName(path.join(process.cwd(), 'test/screenshots/reference')),
      screenshotName: getScreenshotName(path.join(process.cwd(), 'test/screenshots/taken')),
      diffName: getScreenshotName(path.join(process.cwd(), 'test/screenshots/diff')),
      misMatchTolerance: 0.01,
    }),
    viewportChangePause: 800,
    // viewports: []
  },
  viewports: {
    mobile: [
      {width: 360, height: 640}
    ],
    other: [
      {width: 768, height: 1024},
      {width: 1200, height: 720},
      {width: 1800, height: 900}
    ]
  },
  //
  // Framework you want to run your specs with.
  // The following are supported: Mocha, Jasmine, and Cucumber
  // see also: http://webdriver.io/guide/testrunner/frameworks.html
  //
  // Make sure you have the wdio adapter package for the specific framework installed
  // before running any tests.
  framework: 'mocha',
  //
  // Test reporter for stdout.
  // The only one supported by default is 'dot'
  // see also: http://webdriver.io/guide/testrunner/reporters.html
  reporters: ['spec', 'allure'],
  reporterOptions: {
    allure: {
      outputDir: 'test/allure-results'
    }
  },

  //
  // Options to be passed to Mocha.
  // See the full list at http://mochajs.org/
  mochaOpts: {
    ui: 'bdd',
    timeout: 40000,
    retries: 1
  },
  //
  // =====
  // Hooks
  // =====
  // WebdriverIO provides several hooks you can use to interfere with the test process in order to enhance
  // it and to build services around it. You can either apply a single function or an array of
  // methods to it. If one of them returns with a promise, WebdriverIO will wait until that promise got
  // resolved to continue.
  //
  // Gets executed once before all workers get launched.
  // onPrepare: function (config, capabilities) {
  // },
  //
  // Gets executed just before initialising the webdriver session and test framework. It allows you
  // to manipulate configurations depending on the capability or spec.
  // beforeSession: function (config, capabilities, specs) {
  // },
  //
  // Gets executed before test execution begins. At this point you can access all global
  // variables, such as `browser`. It is the perfect place to define custom commands.
  before: function(capabilities, specs) {
    let chai = require('chai');
    let dirtyChai = require('dirty-chai');
    global.expect = chai.expect;
    chai.use(dirtyChai);

    /**
     * Assigns different viewport sizess due to Mac Chrome minimum width being 400px
     * Details at https://camppacific.atlassian.net/wiki/spaces/TT/pages/1283718/Troubleshooting
     * @return {Array} Array of browser sizes in pixels
     */
    let getBrowserSizes = function() {
      let caps = browser.desiredCapabilities;
      let sets = browser.options.viewports;
      let sizes;

      if (caps.browserName === 'chrome') {
        if (caps.chromeOptions && caps.chromeOptions.mobileEmulation) {
          sizes = sets.mobile;
        } else {
          sizes = sets.other;
        }
      } else {
        sizes = sets.mobile.concat(sets.other);
      }

      return sizes;
    };

    // Visual regression functions
    assertDiff = function(results) {
      results.forEach(function(result) {
        expect(result.isExactSameImage, 'Image isn\'t the same').to.be.true();
      });
    };

    compareElement = function(selector, options) {
      let report;

      options.viewports = getBrowserSizes();
      report = browser.checkElement(selector, options);
      assertDiff(report);
    };

    compareDocument = function(options) {
      let report;

      options.viewports = getBrowserSizes();
      report = browser.checkDocument(options);
      assertDiff(report);
    };
  },
  //
  // Hook that gets executed before the suite starts
  // beforeSuite: function (suite) {
  // },
  //
  // Hook that gets executed _before_ a hook within the suite starts (e.g. runs before calling
  // beforeEach in Mocha)
  // beforeHook: function () {
  // },
  //
  // Hook that gets executed _after_ a hook within the suite starts (e.g. runs after calling
  // afterEach in Mocha)
  // afterHook: function () {
  // },
  //
  // Function to be executed before a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
  // beforeTest: function (test) {
  // },
  //
  // Runs before a WebdriverIO command gets executed.
  // beforeCommand: function (commandName, args) {
  // },
  //
  // Runs after a WebdriverIO command gets executed
  // afterCommand: function (commandName, args, result, error) {
  // },
  //
  // Function to be executed after a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
  // afterTest: function (test) {
  // },
  //
  // Hook that gets executed after the suite has ended
  // afterSuite: function (suite) {
  // },
  //
  // Gets executed after all tests are done. You still have access to all global variables from
  // the test.
  // after: function (result, capabilities, specs) {
  // },
  //
  // Gets executed right after terminating the webdriver session.
  // afterSession: function (config, capabilities, specs) {
  // },
  //
  // Gets executed after all workers got shut down and the process is about to exit. It is not
  // possible to defer the end of the process using a promise.
  // onComplete: function(exitCode) {
  // }
};
