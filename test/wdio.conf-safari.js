// https://itisatechiesworld.wordpress.com/2015/04/15/steps-to-get-selenium-webdriver-running-on-safari-browser/

let merge = require('deepmerge');
let wdioConf = require('./wdio.conf.js');

exports.config = merge(wdioConf.config, {
  capabilities: [{
    browserName: 'safari',
    maxInstances: 1
    // specs: [
    // ],
    // exclude: [
    // ]
  }],
  seleniumInstallArgs: {
    version: '2.53.1'
  },
  seleniumArgs: {
    version: '2.53.1'
  },
});
