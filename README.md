## Automatic Testing Template (WebDriverIO and Sitespeed.io)

### Description:
This is a barebones WebDriverIO and Sitespeed.io setup that contains generic configuration and test files.  When creating new test setup for a project, this template should be copied over.  If the template needs to be updated, then it should be updated here.  This template should contain generic settings and scripts only.

https://camppacific.atlassian.net/wiki/display/TT/Front+End+Testing


This template contains multiple configuration files:
* `wdio.conf.js` (for testing on your local machine)
* `wdio-remote-cbt.conf.js` (to test on crossbrowsertesting.com)
* `wdio.conf-opera.js` (opera tests cannot run at the same time as chrome)
* `wdio.conf-safari.js` (our safari tests currently use selenium 2.x)

### Structure
`util` folder contains the test definitions and should be organized by type of test.  For instance, links, forms, etc..  If test definitions require specific configurations, it should be noted in its comment header.


`settings` folder contains your test configurations for our generic tests and should be divided by feature, section, or page.  Here is the structure of the configuration for the generic tests already created:
```
var tests = {
  // This is the name of the test you want to run. e.g. contact links
  suite: 'test name',

  // This is the selector to target the element. e.g. .contact-block__contact-info a
  selector: '.css_selector',

  // This is an array of test cases and should contain at least a description and an index.
  // The index is required because the test will loop through the elements that match the selector.
  cases: [
    {
      description: 'a should match b',
      index: 0,
      ...
    }
  ]
};

module.exports = tests;
```

For additional properties for individual cases, please see the test definitions under `util` folder.

`specs` folder contains the test scripts.  This is where WebDriverIO will look for instructions for running the tests.  Each script should have `include` statements to import the appropriate test definitions and settings you want to use to run the tests.  Each script should correspond to a configuration and should be named the same.  Test scripts should be divided into folders `functional` or `visual` depending on the type of test it's supposed to be.

`package.json` contains all the WebDriverIO dependencies.

`wdio.conf.js` is a configuration file so you can test on your local machine.  Most of the configurations should be set up for you, but there will be some that will need to be modified.

`wdio-remote-cbt.conf.js` is a configuration file so you can test on crossbrowsertesting.com.  See [Remote Services](https://camppacific.atlassian.net/wiki/display/TT/Background+Information) for instructions on how to use it.

`sitespeed-cfg.json` is a configuration file for running Sitespeed.io. See [Sitespeed.io docs](https://www.sitespeed.io/documentation/sitespeed.io/configuration/#the-options) for a description of each option.

`sitespeed-urls.txt` is a text file for listing all URLs to be tested by Sitespeed.io. Each line should be its own URL.

`allure-results`, `allure-report`, and `sitespeed-result` are folders created during the tests. They are automatically deleted before starting a new test run.

### How to use this template:
1. Create a folder in your project called `test`.
2. Copy the contents of `test` from this repo to the `test` folder you just created.  Copy the `.env` file to the top level project folder.
3. Copy `devDependencies` from this `package.json` to the `package.json` of your project. Install the new packages with `npm install`.
4. Update `wdio.conf.js` for your project as needed:
  * Change `baseUrl` to either the staging link or your localhost url.
  * Under `capabilities`, you may want to add browser specific tests or exclude specific tests, although it's not required.
5. Update `wdio-remote-cbt.conf.js` for your project as needed:
  * Change `baseUrl` to either the staging link.
  * Under `capabilities`, update the browser, version, specific tests to run to exclude.
6. Update `sitespeed-cfg.json` as needed. Crawler depth can be set to 1 to only test the exact URLs. Iterations controls how many times it will visit a single URL. Remote services like Webpagetest and gpsi can be excluded to reduce test times.
7. Update `sitespeed-urls.txt` as needed. Each line should be its own URL.
8. Update your `gulpfile.js` by copying over the testing tasks. If your project is not using the same packages, the `require` lines at the top may need to be copied too. If different tasks are needed, refer to gulp-webdriver instructions [here](http://webdriver.io/guide/plugins/gulp-webdriver.html) on how to set that up.
9. The settings, specs, and util files should be generic, but if you need to you can make changes as needed per project.  Changes should be made in settings first before attempting to make changes in the other files.
10. If there are tests that aren't covered by the ones in this template, you will need to add it yourself.


### Dev Process:
* Developers are responsible for running tests on their local environment to ensure stable code.  Tests should be run before committing and pushing to gitlab.
* QA team is responsible for running tests on crossbrowsertesting.com service.  These tests should point to the staging site to be evaluated.


### How to run a test:
In your terminal, cd to the top level of your project (where the `Gulpfile` is located).

#### Testing locally
1. Make sure your local server is running
2. Disable click and scroll syncing for BrowserSync
3. Run `$ gulp test` for running all tests
4. Otherwise `$ gulp test --suite <functional/visual/etc>` for targeting specific suites as defined in `wdio.conf.js`
5. `<Ctrl+C>` to close the Allure server when done viewing the test report

#### Testing the Opera browser
See [here](https://camppacific.atlassian.net/wiki/spaces/TT/pages/6160481/Opera+Driver) for details on why this is handled differently and follow the instructions on setting up your machine.

1. Run `$ operadriver --port=4444 --verbose` to start the Opera driver.
2. Run `$ gulp test --browser opera` to run the test in opera.
4. Otherwise `$ gulp test --browser opera --suite <functional/visual/etc>` for targeting specific suites.

*Important*: Turn off operadriver when you want to test Chrome, Firefox, and Safari.

### Testing the Safari browser
Run `$ gulp test --browser safari --suite <functional/visual/etc>`


#### Testing remotely on CrossBrowserTesting
`$ gulp test --browser cbt` to use the `wdio-remote-cbt.conf.js` configuration
`$ gulp test --browser cbt --suite <functional/visual/etc>`

#### Running Sitespeed.io
`$ gulp sitespeed` will run and generate HTML results in `test/sitespeed-result`. There is no server requirement unlike Allure. Each HTML file can be viewed individually.
