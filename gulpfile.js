const gulp = require('gulp');
const exec = require('child_process').exec;
const del = require('del');
const argv = require('yargs').argv;
const loadPlugins = require('gulp-load-plugins');
const $ = loadPlugins();


/**
 * TESTING
 * wdio default: $ gulp test --suite <functional/visual/etc>
 * wdio alternate: $ gulp test --browser <opera/safari/cbt> --suite <functional/visual/etc>
 */
gulp.task('clean:test', function() {
  return del([
    'test/allure-results',
    'test/allure-report',
    'test/logs',
    'test/sitespeed-result'
  ]);
});

gulp.task('webdriverio', function() {
  let config = (argv.cbt) ? 'test/wdio-remote-cbt.conf.js' : 'test/wdio.conf.js';
  let options = {};
  if (argv.suite) {
    options.suite = argv.suite;
  }

  switch (argv.browser) {
  case 'cbt':
    config = 'test/wdio-remote-cbt.conf.js';
    break;
  case 'opera':
    config = 'test/wdio.conf-opera.js';
    break;
  case 'safari':
    config = 'test/wdio.conf-safari.js';
    break;
  default:
    config = 'test/wdio.conf.js';
  }

  return gulp.src(config)
    .pipe($.plumber())
    .pipe($.webdriver(options))
    .pipe($.plumber.stop());
});

gulp.task('allure:generate', function(cb) {
  let allure = exec('node_modules/.bin/allure generate --clean -o test/allure-report test/allure-results', (error, stdout, stderr) => {
    cb(error);
  });
  allure.stdout.pipe(process.stdout);
  allure.stderr.pipe(process.stderr);
});

gulp.task('allure:report', function(cb) {
  let allure = exec('node_modules/.bin/allure open test/allure-report', (error, stdout, stderr) => {
    cb();
  });
  allure.stdout.pipe(process.stdout);
  allure.stderr.pipe(process.stderr);
});

gulp.task('allure', $.sequence(
  'allure:generate',
  'allure:report'
));

gulp.task('test', $.sequence(
  'clean:test',
  'webdriverio',
  'allure'
));

gulp.task('sitespeed', function() {
  let folder = 'test/sitespeed-result';
  let config = 'test/sitespeed-cfg.json';
  let urls = 'test/sitespeed-urls.txt';
  let sitespeed = exec(`node_modules/.bin/sitespeed.io --outputFolder ${folder} --config ${config} ${urls}`);
  sitespeed.stdout.pipe(process.stdout);
  sitespeed.stderr.pipe(process.stderr);
});
